FROM python:3.8

RUN apt-get update -y && apt-get upgrade -y \
    gdal-bin

RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/

RUN pip install --upgrade pip
RUN pip install -r requirements.txt