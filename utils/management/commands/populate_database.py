import random

from django.core.management import BaseCommand

from utils.factories import ArtistFactory, EventFactory


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--events_num', type=int, help="number of events to be created")
        parser.add_argument('--artists_num', type=int, help="number of artists to be created")

    def handle(self, *args, **options):
        events_num = options['events_num']
        artists_num = options['artists_num']

        artists = []
        for number in range(artists_num):
            artist = ArtistFactory()
            artists.append(artist)

        for number in range(events_num):
            event = EventFactory()
            event_artists = random.choices(artists, k=random.randint(1, artists_num))
            event.artists.add(*event_artists)
