import factory
import pytz

from events.models import Artist, Event


class ArtistFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Artist

    name = factory.Faker('name')
    genre = factory.Faker('word')


class EventFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Event

    name = factory.Faker('sentence')
    description = factory.Faker('text')
    start = factory.Faker('date_time', tzinfo=pytz.utc)

    # TODO fake geolocation data
