from django.db import models


class Artist(models.Model):
    name = models.CharField(max_length=64)
    genre = models.CharField(max_length=32)

    def __str__(self):
        return self.name
