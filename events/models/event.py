from django.contrib.gis.db.models import PointField
from django.contrib.gis.geos import Point

from django.db import models
from django_google_maps import fields as map_fields

from events.models import Artist


class Event(models.Model):
    name = models.CharField(max_length=128, null=False, blank=False)
    description = models.TextField(null=False, blank=False)
    start = models.DateTimeField(null=False, blank=False)
    artists = models.ManyToManyField(Artist, related_name='events')
    address = map_fields.AddressField(max_length=200, null=True, blank=True)
    geolocation = map_fields.GeoLocationField(max_length=100, null=True, blank=True)
    lat_lon = PointField(null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.geolocation:
            self.lat_lon = Point(self.geolocation.lon, self.geolocation.lat)
        super(Event, self).save(*args, **kwargs)
