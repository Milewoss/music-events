from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from events.models import Event, Artist


class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = '__all__'


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ('id', 'name', 'description', 'start', )


class EventFullListSerializer(serializers.ModelSerializer):
    artists = ArtistSerializer(many=True)

    class Meta:
        model = Event
        exclude = ('lat_lon', )


class EventFullSerializer(serializers.ModelSerializer):
    artists = PrimaryKeyRelatedField(many=True, queryset=Artist.objects.all())

    class Meta:
        model = Event
        fields = '__all__'


class ArtistFullListSerializer(serializers.ModelSerializer):
    events = EventSerializer(many=True)

    class Meta:
        model = Artist
        fields = '__all__'


class ArtistFullSerializer(serializers.ModelSerializer):
    events = PrimaryKeyRelatedField(many=True, queryset=Event.objects.all())

    class Meta:
        model = Artist
        fields = '__all__'
