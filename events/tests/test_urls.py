import json
from datetime import datetime
from functools import partial
from parameterized import parameterized
from typing import Any, List

import pytz
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase

from events.models import Event, Artist
from utils.factories import EventFactory, ArtistFactory


event_data_provider = [
    [
        True,
        {
            'name': 'test_event',
            'description': 'test_genre',
            'start': datetime.now(tz=pytz.utc),
        },
        status.HTTP_400_BAD_REQUEST,
    ],
    [
        True,
        {
            'name': 'test_event',
            'description': 'test_genre',
            'start': datetime.now(tz=pytz.utc),
            'artists': []
        },
        status.HTTP_201_CREATED,
    ],
    [
        False,
        {
            'name': 'test_event',
            'description': 'test_genre',
            'start': datetime.now(tz=pytz.utc),
            'artists': []
        },
        status.HTTP_403_FORBIDDEN,
    ]
]

artist_data_provider = [
    [
        True,
        {
            'name': 'random artist',
            'genre': 'random genre',
        },
        status.HTTP_400_BAD_REQUEST,
    ],
    [
        True,
        {
            'name': 'random artist',
            'genre': 'random genre',
            'events': []
        },
        status.HTTP_201_CREATED,
    ],
    [
        False,
        {
            'name': 'random artist',
            'genre': 'random genre',
            'events': []
        },
        status.HTTP_403_FORBIDDEN,
    ],
]


class APITestUrlHelper:

    client: Any
    url: str

    def _test_get(self, orders_by: List[str]) -> None:
        for order_by in orders_by:
            response = partial(self.client.get, f'{self.url}?ordering={order_by}')()
            response_json = json.loads(s=response.content)
            assert response_json['count'] == 30
            assert len(response_json['results']) == settings.REST_FRAMEWORK['PAGE_SIZE']
            assert f"{self.url}?ordering={order_by}&page=2" in response_json['next']
            assert response_json['previous'] is None
            last_value = response_json['results'][0][order_by.replace('-', '')]
            for row in response_json['results']:
                this_row_value = row[order_by.replace('-', '')]
                if order_by[0] == '-':
                    assert max(last_value, this_row_value) == last_value
                    assert min(last_value, this_row_value) == this_row_value
                else:
                    assert max(last_value, this_row_value) == this_row_value
                    assert min(last_value, this_row_value) == last_value
                last_value = row[order_by.replace('-', '')]


class EventAPITests(APITestCase, APITestUrlHelper):
    url = '/api/events/'
    model = Event

    def setUp(self) -> None:
        super().setUp()
        self.user = User.objects.create_user(
            username='james', email='james@doe.com', password='secret')
        self.artist = Artist.objects.create(name='test_artist', genre='test_genre')

    def _create_records(self):
        for i in range(0, 30):
            EventFactory()

    def test_get(self) -> None:
        self._create_records()
        self._test_get(orders_by=['id', '-id', 'name', '-name'])

    @parameterized.expand(event_data_provider)
    def test_post_login(self, login, data, status_code) -> None:
        if login:
            self.client.login(username='james', password='secret')
        if 'artists' in data.keys():
            data['artists'] = [self.artist.id]
        response = self.client.post(path=self.url, data=data, format='json')
        self.assertEqual(response.status_code, status_code)
        if login:
            self.client.logout()


class ArtistAPITests(APITestCase, APITestUrlHelper):
    url = '/api/artists/'
    model = Artist

    def setUp(self) -> None:
        super().setUp()
        self.user = User.objects.create_user(
            username='james', email='james@doe.com', password='secret')
        self.event = Event.objects.create(
            name='test event',
            description='test description',
            start=datetime.now(tz=pytz.utc)
        )

    def _create_records(self) -> None:
        for i in range(0, 30):
            ArtistFactory()

    def test_get(self) -> None:
        self._create_records()
        self._test_get(orders_by=['id', '-id', 'name', '-name', 'genre', '-genre'])

    @parameterized.expand(artist_data_provider)
    def test_post_login(self, login, data, status_code) -> None:
        if login:
            self.client.login(username='james', password='secret')
        if 'events' in data.keys():
            data['events'] = [self.event.id]
        response = self.client.post(path=self.url, data=data, format='json')
        self.assertEqual(response.status_code, status_code)
        if login:
            self.client.logout()
