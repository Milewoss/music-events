from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django_filters.rest_framework import DjangoFilterBackend, FilterSet, CharFilter
from rest_framework import viewsets, permissions, mixins
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.response import Response
from geopy.geocoders import Nominatim
from django.conf import settings

from events.models import Event, Artist
from events.serializers import EventFullSerializer, ArtistFullSerializer, EventFullListSerializer, ArtistSerializer,\
    ArtistFullListSerializer


class ListingFilter(FilterSet):
    city = CharFilter(field_name='address', method='filter_city', lookup_expr='write city')

    def filter_city(self, queryset, name, value):
        if not value:
            return queryset

        nominatim_geocoder = Nominatim(user_agent="music_events")
        lat_lon = nominatim_geocoder.geocode(query={'city': value})
        ref_location = Point(lat_lon.longitude, lat_lon.latitude, srid=4326)
        queryset = queryset.filter(
            lat_lon__distance_lte=(ref_location, D(m=settings.API_EVENTS_FILTER_METERS_DISTANCE))).annotate(
            distance=Distance("lat_lon", ref_location)).order_by("distance")
        return queryset


class EventViewSet(viewsets.ModelViewSet, mixins.ListModelMixin):

    queryset = Event.objects.all()
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    filter_backends = [DjangoFilterBackend, OrderingFilter, ]
    filterset_class = ListingFilter
    serializer_classes = {
        'list': EventFullListSerializer,
    }
    default_serializer_class = EventFullSerializer
    ordering_fields = ['id', 'name', ]
    ordering = ['id', 'name', ]

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.default_serializer_class)


class ArtistViewSet(viewsets.ModelViewSet, mixins.ListModelMixin):
    queryset = Artist.objects.all()
    serializer_classes = {
        'list': ArtistFullListSerializer,
    }
    default_serializer_class = ArtistFullSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    filter_backends = [DjangoFilterBackend, OrderingFilter, SearchFilter]
    search_fields = ['name', 'genre']
    ordering_fields = ['id', 'name', 'genre']
    ordering = ['id', 'name', 'genre']

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.default_serializer_class)

    @action(detail=True, methods=['GET'])
    def get_common_artists(self, request, pk=None):
        artist = self.get_object()
        artist_events = Event.objects.filter(artists=artist)
        common_artists = Artist.objects.filter(events__in=artist_events).distinct().exclude(id=artist.id)
        serializer = ArtistSerializer(common_artists, many=True)
        return Response(serializer.data)
