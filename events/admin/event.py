from django.contrib import admin
from django.forms import TextInput
from django_google_maps import widgets as map_widgets
from django_google_maps import fields as map_fields
from events.models import Event


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('name', )
    filter_horizontal = ('artists', )
    exclude = ('lat_lon', )

    formfield_overrides = {
        map_fields.AddressField: {
            'widget': map_widgets.GoogleMapsAddressWidget
        },
        map_fields.GeoLocationField: {
            'widget': TextInput(attrs={
                'readonly': 'readonly'
            })
        },
    }
