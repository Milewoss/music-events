# Music Event recruit application app

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)

## General info
Simple music app to test own ability to use the django rest framework, write tests, and use Django in general.

## Technologies
* Python 3.8
* Django 3.1.4
* Django REST Framework 3.11.1
* Docker 19.03.13

## Setup
* Clone or download the repo
* Open the project directory
* Create `.env` file from `.env.example` and remember to add Google V3 API key
* `docker-compose up --build -d` to build the project
* `docker-compose logs` or `docker ps` to check if docker containers standing
* `docker-compose exec app bash` to open the docker container with project (when in project directory)
* `python manage.py populate_database --events_num 20 --artists_num 10` to populate the database with sample objects. 
The number of events and artists can be any
* `python manage.py createsuperuser` To create admin user. Follow the wizard steps

## Features
* `localhost:8000/admin` admin panel url
* `localhost:8000/api/events/` API endpoint for events
* `localhost:8000/api/artists/` API endpoint for artists
* `localhost:8000/api/artists/<artis_id>/get_common_artists/` API endpoint for artists list that played in the same
events as chosen artist
